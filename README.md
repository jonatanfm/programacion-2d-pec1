# PEC1

Implementación de un juego de aventuras, más concretamente la lógica de combate del mítico juego _Monkey Island_.

## Cómo jugar al juego

Es tan sencillo como empezar una nueva partida y aleatoriamente el juego elegirá quien comienza insultando al otro. En caso de que el jugador tenga que insultar o de responder a un insulto, siempre le aparecerán las opciones que tiene disponibles en la caja situada en la parte inferior de la pantalla, mediante las flechas que aparecerán en la parte izquierda de la caja, se podrá navegar por todas las opciones disponibles en ese momento.

[Aquí se puede ver un gameplay de 2 minutos.](gameplay.mp4)

## Implementación del juego

El juego se compone de tres escenas: MainMenu, Game y EndMenu. 

### Escena _MainMenu_

Se compone de:
- **MainCamera**: GameObject principal con el componente Camera. También contiene un AudioSource que contiene el sonido de background, y un script para que no se destruya al cambiar de escena, así la música continuará sin interrupciones.
- **ScreenNavigator**: Script principal para navegar entre escenas, tiene las funciones para cambiar de escena.
- **MenuBackground**: Prefab que contiene los sprites del fondo de pantalla, es una modificación del prefab GameBackground pero ocultando la parte donde se desarrolla la acción.
- **Canvas**: Contiene el título del juego y dos botones para empezar una partida o para salir del juego
- **EventSystem**: EventSystem que se crea por defecto al crear un canvas en la escena.

### Escena _Game_

Se compone de:
- **MainCamera**: GameObject principal con el componente Camera. También contiene un AudioSource que contiene el sonido de background, y un script para que no se destruya al cambiar de escena, así la música continuará sin interrupciones.
- **ScreenNavigator**: Script principal para navegar entre escenas, tiene las funciones para cambiar de escena.
- **Background**: Prefab que contiene los sprites del fondo de pantalla.
- **Canvas**: Contiene: 
	- ***VoiceTextBox***: Caja de texto para cuando uno de los personajes habla, la posición Y siempre es la misma pero la posición X varia según el personaje hablando.
	- ***Scroller***: Script que se ocupa de que la lista de opciones disponible tenga función de _scroll_. Mediante dos botones (_UpArrow_ y _DownArrow_) se modifica la posición Y de la lista de opciones para así poder ver todas las opciones.
	- ***MaskImage/OptionsList***: GameObject vacío que rellenará el script _GameManager_ con la lista de opciones (prefab _Option_) disponible.
	- ***ArrowButtons***: Contenedor de los botones _UpArrow_ y _DownArrow_ mencionados anteriormente.
- **EventSystem**: EventSystem que se crea por defecto al crear un canvas en la escena.
- **Player**: GameObject con las animaciones y sprites del Player, también contiene el componente _CharacterFightController_ script que se compone de tres funciones:
	- *DoFight*: Recibe un bool indicando si ha ganado o perdido el turno e inicia la corrutina _MoveForward_. También devuelve el tiempo en segundo que durará la corrutina _MoveForward_.
	- *MoveForward*: Recibe un bool indicando si ha ganado o perdido el turno, inicia la animación de caminar y avanza hasta la posición donde se llevará a cabo el combate. Al llegar a esa posición en el tiempo designado finaliza la animación de caminar e inicia la corrutina _AttackOrHurt_.
	- *AttackOrHurt*: Recibe un bool indicando si ha ganado o perdido el turno, si ha ganado inicia la animación de atacar y el sonido de ataque, si ha perdido inicia la animación de que ha sido herido y lo desplaza hacia atrás, el acabar finaliza la animación volviendo al estado de _idle_.
- **Enemy**: GameObject similar a _Player_ pero modificando los sprites, animaciones, sonidos y tiempos de ataque, defensa, etc.
- **GameManager**:Es el script más importante del juego ya que se encarga de toda la lógica de este. Al empezar carga en memoria todos los insultos desde un JSON, y empieza una _Round_. Una ronda se compone de: _Jugador preguntando_, _Jugador contestando_, _Turno actual_, _Pregunta seleccionada_, _Respuesta seleccionada_;\
Durante los tres turnos que componen una ronda (Pregunta, Respuesta, Batalla), las variables se van _seteando_ y al final de la ronda se puede saber quien ha ganado la ronda. Con esta información llama a la función _DoFight_ de ambos _Player_ y _Enemy_ indicando quien ha ganado. Y por último llama al script _ScoreManager_ para incrementar la puntuación de los jugadores. Si _ScoreManager_ no indica que se ha acabado la partida, se crea una nueva ronda pero marcando como jugador que pregunta al ganador de la ronda anterior; y si indica que ha finalizado, mediante el _ScreenNavigator_ se carga la escena _EndMenu_. 
- **ScoreManager**: Guarda la información del marcador de la pelea. En caso de cambiar de escena hacía el _EndMenu_, no se destruirá puesto que el _EndMenu_ está esperandolo.

### Escena _EndMenu_

Se compone de:
- **MainCamera**: GameObject principal con el componente Camera. También contiene un AudioSource que contiene el sonido de background, y un script para que no se destruya al cambiar de escena, así la música continuará sin interrupciones.
- **ScreenNavigator**: Script principal para navegar entre escenas, tiene las funciones para cambiar de escena.
- **MenuBackground**: Prefab que contiene los sprites del fondo de pantalla, es una modificación del prefab GameBackground pero ocultando la parte donde se desarolla la acción.
- **Canvas**: Contiene el mensaje que indica quien ha ganado la partida y dos botones para volver al menú principal o para salir del juego.
- **EventSystem**: EventSystem que se crea por defecto al crear un canvas en la escena.
- **ResultManager**: Script que se encarga de buscar el GameObject _ScoreManager_ que viene heredado de la escena _Game_ y que contiene el resultado de la partida. En base al resultado de la partida el script actualiza el texto con el mensaje del resultado y destruye la instancia de _ScoreManager_ para que no se mantenga al volver a la escena _MainMenu_.


## Créditos

### Música
_Doom and Gloom_ by Steve O'Connell provided by [freesoundtrackmusic.com](https://freesoundtrackmusic.com)

### Sprites
_GothicVania Church_ by Ansimuz provided by [itch.io](https://ansimuz.itch.io/gothicvania-church-pack)

### Fuentes
_Alagard_ by Hewett Tsoi provided by [dafont.com](https://www.dafont.com/alagard.font)

_Retro Gaming_ by Daymarius provided by [dafont.com](https://www.dafont.com/retro-gaming.font)