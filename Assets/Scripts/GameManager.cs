﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    [SerializeField]
    private GameObject optionPrefab;
    [SerializeField]
    private GameObject optionList;
    [SerializeField]
    private Scroller scroller;
    [SerializeField]
    private string gameOptionsPath;

    [SerializeField]
    private GameObject player;
    [SerializeField]
    private GameObject enemy;

    [SerializeField]
    private TextMeshProUGUI voiceTextBox;

    [SerializeField]
    private int waitingTime = 3;

    [SerializeField]
    private ScoreManager scoreManager;

    [SerializeField]
    private ScreenNavigator screenNavigator;

    private float optionHeight;
    private float optionListWidth;
    private float optionListY;
    private float voiceTextBoxY;
    private GameOptions gameOptions;
    const int optionsToShow = 5;

    private Round round;

    void Start() {
        InitVars();

        StartRound(Random.Range(0, 2).Equals(0));
    }

    private void InitVars() {
        optionHeight = optionPrefab.GetComponent<RectTransform>().sizeDelta.y;
        optionListWidth = optionList.GetComponent<RectTransform>().sizeDelta.x;
        optionListY = optionList.GetComponent<RectTransform>().localPosition.y;
        voiceTextBoxY = voiceTextBox.GetComponent<RectTransform>().localPosition.y;

        LoadQuestions();
    }

    // Loads a JSON file into gameOptions
    private void LoadQuestions() {
        var json = Resources.Load<TextAsset>(gameOptionsPath);
        gameOptions = JsonUtility.FromJson<GameOptions>(json.ToString());
    }

    // Creates a round with the asking and answering players as the round is and executes the turn
    private void StartRound(bool isPlayerAsking) {
        if (isPlayerAsking) {
            round = new Round(Round.Character.Player, Round.Character.Enemy);
        } else {
            round = new Round(Round.Character.Enemy, Round.Character.Player);
        }

        ExecuteTurn();
    }

    private void ExecuteTurn() {
        if (round.GetCurrentTurn() == Round.Turn.Fight) {
            StartCoroutine(ExecuteFightTurn());
            return;
        }

        string[] options = GetOptionsOnCurrentTurn();

        if (round.GetActiveCharacter().Equals(Round.Character.Player)) {
            ExecutePlayerTurn(options);
        } else {
            ExecuteEnemyTurn(options);
        }
    }

    private IEnumerator ExecuteFightTurn() {
        // Update the score of the match
        if (round.DidCharacterWin(Round.Character.Player)) {
            scoreManager.IncreasePlayerScore();
        } else {
            scoreManager.IncreaseEnemyScore();
        }

        // Run player and enemy animations for the fight
        float playerFightingTime = player.GetComponent<CharacterFightController>().DoFight(round.DidCharacterWin(Round.Character.Player));
        float enemyFightingTime = enemy.GetComponent<CharacterFightController>().DoFight(round.DidCharacterWin(Round.Character.Enemy));

        // Wait until the fight animation is over
        yield return new WaitForSeconds(Mathf.Max(playerFightingTime, enemyFightingTime));

        // Check if the battle has finished, load EndMenu scene if so and starts new round if not
        if (scoreManager.HasBattleFinished()) {
            screenNavigator.LoadEndMenu();
        } else {
            StartRound(round.DidCharacterWin(Round.Character.Player));
        }
    }

    private void ExecuteEnemyTurn(string[] options) {
        int optionSelected = Random.Range(0, options.Length);

        StartCoroutine(DoOption(optionSelected));
    }

    IEnumerator DoOption(int optionSelected) {
        // Update information of the round
        round.SetOptionSelected(optionSelected);

        // Check which GameObject is speaking
        GameObject speakingGameObject = player;
        if (round.GetActiveCharacter().Equals(Round.Character.Enemy)) {
            speakingGameObject = enemy;
        }
        
        // Show the text on top of the GameObject talking
        UpdateVoiceTextBox(GetOptionsOnCurrentTurn()[optionSelected], speakingGameObject);

        // Wait some time to allow the player to read the message
        yield return new WaitForSeconds(waitingTime);

        // Clean the message
        UpdateVoiceTextBox("", speakingGameObject);

        // Move to next turn
        round.NextTurn();
        ExecuteTurn();
    }

    private void ExecutePlayerTurn(string[] options) {
        PopulateOptionsUI(options);
    }

    private void PopulateOptionsUI(string[] options) {
        // Clean optionList children
        while (optionList.transform.childCount > 0) {
            DestroyImmediate(optionList.transform.GetChild(0).gameObject);
        }

        // Create new Option Prefab for each option in options and add them to optionList
        for (int i = 0; i < options.Length; i++) {
            var newOption = Instantiate(optionPrefab, optionList.transform);
            newOption.GetComponent<TextMeshProUGUI>().text = options[i];

            RectTransform newOptionRT = newOption.GetComponent<RectTransform>();
            newOptionRT.position = new Vector2(newOptionRT.position.x, newOptionRT.position.y + (-i * optionHeight));

            int optionIndex = i;
            newOption.GetComponent<Button>().onClick.AddListener(delegate { OptionButtonClicked(optionIndex); });
        }

        // Update size and position of optionList
        optionList.GetComponent<RectTransform>().sizeDelta = new Vector2(optionListWidth, options.Length * optionHeight);
        optionList.transform.localPosition = new Vector2(0, optionListY);

        // Update scroller information
        scroller.UpdateScroller(optionsToShow < options.Length ? optionsToShow : options.Length, options.Length, optionHeight);
    }

    private string[] GetOptionsOnCurrentTurn() {
        string[] options = {};

        if (round.GetCurrentTurn().Equals(Round.Turn.Question)) {
            options = gameOptions.questions;
        } else if (round.GetCurrentTurn().Equals(Round.Turn.Answer)) {
            options = gameOptions.answers;
        }

        return options;
    }

    // Checks the position of the GameObject and moves the textbox to the same X position
    private void UpdateVoiceTextBox(string text, GameObject characterTalking) {
        float newPosX = characterTalking.transform.position.x * (1080 / 20);
        if (newPosX > 600 || newPosX < -600) {
            newPosX = 600 * (newPosX < 0 ? -1 : 1);
        }
        voiceTextBox.GetComponent<RectTransform>().localPosition = new Vector2(newPosX, voiceTextBoxY);
        voiceTextBox.text = text;
    }

    private void OptionButtonClicked(int optionSelected) {
        PopulateOptionsUI(new string[] { });
        StartCoroutine(DoOption(optionSelected));
    }
}
