﻿using UnityEngine;

public class ScoreManager : MonoBehaviour {

    private int playerScore = 0;
    private int enemyScore = 0;

    [SerializeField]
    private int winningScore = 3;

    // Not to be destroyed while changing scenes
    private void Awake() {
        DontDestroyOnLoad(transform.gameObject);
    }

    public void IncreasePlayerScore() {
        playerScore++;
    }

    public void IncreaseEnemyScore() {
        enemyScore++;
    }

    // Returns true if either player or enemy have reach winningScore
    public bool HasBattleFinished() {
        return playerScore >= winningScore || enemyScore >= winningScore;
    }

    public bool DidPlayerWin() {
        return playerScore > enemyScore;
    }
}
