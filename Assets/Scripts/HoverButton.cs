﻿using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class HoverButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

    private TextMeshProUGUI text;

    [SerializeField]
    private Color hoverColor;

    private Color initialColor;

    void Start() {
        text = this.GetComponent<TextMeshProUGUI>();
        initialColor = text.color;
    }

    public void OnPointerEnter(PointerEventData eventData) {
        text.color = hoverColor;
    }

    public void OnPointerExit(PointerEventData eventData) {
        text.color = initialColor; 
    }
}