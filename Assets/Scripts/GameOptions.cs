﻿using System;

// Class to decode the JSON input
[Serializable]
public class GameOptions {
    public string[] questions;
    public string[] answers;
}
