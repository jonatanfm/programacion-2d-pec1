﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scroller : MonoBehaviour {

    private int currentPos = 0;
    private int maxPos = 0;
    private int minPos = 0;
    private float rowHeight = 0.0f;

    [SerializeField]
    private GameObject optionList;
    [SerializeField]
    private GameObject upArrow;
    [SerializeField]
    private GameObject downArrow;
    
    // Updates the initialPosition, the max available positions and the rowHeight.
    // It also disables and enabled the scrollers depending on the list of options visible
    public void UpdateScroller(int initialPos, int maxPos, float rowHeight) {
        currentPos = minPos = initialPos;
        this.maxPos = maxPos;
        this.rowHeight = rowHeight;
        // In case there are more positions to move through, we enable the DownArrow Button
        if (maxPos > initialPos) {
            downArrow.SetActive(true);
        } else {
            upArrow.SetActive(false);
            downArrow.SetActive(false);
        }
    }

    // Function that will update the position Y of the optionList so it will scroll up
    public void GoUp() {
        if (currentPos > minPos) {
            currentPos--;
            optionList.transform.position = new Vector2(optionList.transform.position.x, optionList.transform.position.y - rowHeight);
            if (currentPos == minPos) {
                upArrow.SetActive(false);
            }
            if (!downArrow.activeSelf) {
                downArrow.SetActive(true);
            }
        }
    }

    // Function that will update the position Y of the optionList so it will scroll down
    public void GoDown() {
        if (currentPos < maxPos) {
            currentPos++;
            optionList.transform.position = new Vector2(optionList.transform.position.x, optionList.transform.position.y + rowHeight);
            if (currentPos == maxPos) {
                downArrow.SetActive(false);
            }
            if (!upArrow.activeSelf) {
                upArrow.SetActive(true);
            }
        }
    }
}
