﻿using System.Collections;
using UnityEngine;

public class CharacterFightController : MonoBehaviour {

    const string STATE_WALKING = "walking";
    const string STATE_ATTACKING = "attacking";
    const string STATE_HURT = "hurt";
    
    private Animator animator;
    private AudioSource audioSource;

    [SerializeField]
    private float moveX = 4;

    [SerializeField]
    private float movingTime = 2;

    [SerializeField]
    private float attackingTime = 1.5f;

    [SerializeField]
    private float attackSoundTime = 0.5f;

    [SerializeField]
    private float gettingHurtTime = 1.5f;

    [SerializeField]
    private AudioClip attackAudio;

    void Start() {
        animator = gameObject.GetComponent<Animator>();
        audioSource = gameObject.GetComponent<AudioSource>();
    }
    
    // Starts Coroutine MoveForward and returns the time in seconds the Coroutine will take to finish
    public float DoFight(bool iWon) {
        StartCoroutine(MoveForward(iWon));

        return movingTime + (iWon ? attackingTime : (gettingHurtTime + movingTime));
    }

    // Starts the animation WALKING and starts moving from startingPos to endPos in the movingTime set
    // At the end it stops the animation WALKING and starts the Coroutine AttackOrHurt
    private IEnumerator MoveForward(bool iWon) {
        animator.SetBool(STATE_WALKING, true);

        float elapsedTime = 0;
        Vector2 startingPos = transform.position;
        Vector2 endPos = new Vector2(startingPos.x + moveX, startingPos.y);
        while (elapsedTime < movingTime) {
            transform.position = Vector2.Lerp(startingPos, endPos, (elapsedTime / movingTime));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        animator.SetBool(STATE_WALKING, false);
        transform.position = endPos;

        StartCoroutine(AttackOrHurt(iWon));
    }

    // If the character won the round:
    //      start ATTACKING animation, play attack sound and stop ATTACKING animation
    // If the character didn't win the round:
    //      start HURT animation, move the player two time moveX backwards and stop HURT animation
    private IEnumerator AttackOrHurt(bool iWon) { 
        string state = iWon ? STATE_ATTACKING : STATE_HURT;
        
        if (!iWon) {
            yield return new WaitForSeconds(gettingHurtTime);
            animator.SetBool(state, true);

            float elapsedTime = 0;
            Vector2 startingPos = transform.position;
            Vector2 endPos = new Vector2(startingPos.x - (moveX*2), startingPos.y);
            while (elapsedTime < movingTime) {
                transform.position = Vector2.Lerp(startingPos, endPos, (elapsedTime / movingTime));
                elapsedTime += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            transform.position = endPos;
        } else {
            animator.SetBool(state, true);
            yield return new WaitForSeconds(attackSoundTime);
            audioSource.PlayOneShot(attackAudio);
            yield return new WaitForSeconds(attackingTime - attackSoundTime);
        }

        animator.SetBool(state, false);
    }
}
