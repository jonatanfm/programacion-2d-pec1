﻿using System;

public class Round {

    public enum Character { Player, Enemy };
    public enum Turn { Question, Answer, Fight };

    private Character askingCharacter;
    private Character answeringCharacter;

    private Turn currentTurn = Turn.Question;

    private int questionOption;
    private int answerOption;

    public Round(Character askingCharacter, Character answeringCharacter) {
        this.askingCharacter = askingCharacter;
        this.answeringCharacter = answeringCharacter;
    }

    public Turn GetCurrentTurn() {
        return currentTurn;
    }

    public void NextTurn() {
        int tmpTurn = (int)currentTurn;
        tmpTurn++;
        currentTurn = (Turn)tmpTurn;
    }

    public Character GetActiveCharacter() {
        Character activeCharacter = askingCharacter;

        if (!currentTurn.Equals(Turn.Question)) {
            activeCharacter = answeringCharacter;
        }

        return activeCharacter;
    }

    public void SetOptionSelected(int optionSelected) {
        if (currentTurn.Equals(Turn.Question)) {
            questionOption = optionSelected;
        } else if (currentTurn.Equals(Turn.Answer)) {
            answerOption = optionSelected;
        }
    }

    // Return true if
    //   Player asks and the answer doesn't match the question
    //   Player answers and the answer does match the question
    public bool DidCharacterWin(Character character) {
        return (character.Equals(askingCharacter) && !questionOption.Equals(answerOption)) ||
                (character.Equals(answeringCharacter) && questionOption.Equals(answerOption));
    }
}
