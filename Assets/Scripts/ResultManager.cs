﻿using UnityEngine;
using TMPro;

public class ResultManager : MonoBehaviour {

    private GameObject scoreManager;

    [SerializeField]
    private TextMeshProUGUI resultText;

    const string WIN_MESSAGE = "¡Enhorabuena! ¡Has ganado!";
    const string LOSE_MESSAGE = "¡Oh, lo siento! Has perdido...";

    // Gets the GameObject ScoreManager, checks the score and destroys the object
    void Start() {
        scoreManager = GameObject.Find("ScoreManager");

        if (scoreManager.GetComponent<ScoreManager>().DidPlayerWin()) {
            resultText.text = WIN_MESSAGE;
        } else {
            resultText.text = LOSE_MESSAGE;
        }

        Destroy(scoreManager);
    }
}
