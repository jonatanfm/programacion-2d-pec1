﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ScreenNavigator : MonoBehaviour {

    public void QuitGame() {
        Application.Quit();
    }

    public void LoadMainMenu() {
        SceneManager.LoadScene(0);
    }

    public void LoadGameScreen() {
        SceneManager.LoadScene(1);
    }

    public void LoadEndMenu() {
        SceneManager.LoadScene(2);
    }
}
