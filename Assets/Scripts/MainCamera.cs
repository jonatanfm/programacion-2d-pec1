﻿using UnityEngine;

public class MainCamera : MonoBehaviour {

    // Never to be destroyed
    private void Awake() {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("MainCamera");

        if (objs.Length > 1) {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(transform.gameObject);
    }
}
